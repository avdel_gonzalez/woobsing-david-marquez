<?php

namespace App\Http\Middleware;

use Closure;

class LastSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        
        $last_session = new \DateTime($user->last_session);
        $interval = date_diff($last_session, now());
        if((int)$interval->format('%h') > 24){
            return redirect()->route('sessions');
        } else {
            return redirect()->route('dashboard');
        }   
        return $next($request);     
    }
}
