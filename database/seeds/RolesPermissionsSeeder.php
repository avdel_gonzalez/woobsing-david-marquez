<?php

use Illuminate\Database\Seeder;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin',
            ],
            [
                'name' => 'Regular',
            ]
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'read',
            ],
            [
                'name' => 'create',
            ],
            [
                'name' => 'edit',
            ],
            [
                'name' => 'delete',
            ],
        ]);

        DB::table('role_permission')->insert([
            [
                'role_id' => '1',
                'permission_id' => '1'
            ],
            [
                'role_id' => '1',
                'permission_id' => '2'
            ],
            [
                'role_id' => '1',
                'permission_id' => '3'
            ],
            [
                'role_id' => '1',
                'permission_id' => '4'
            ],
            [
                'role_id' => '2',
                'permission_id' => '1'
            ],
            [
                'role_id' => '2',
                'permission_id' => '3'
            ],
        ]);
    }
}
