<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'David Felipe Márquez González',
            'email' => 'davidmarquez.dev@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '3217949964',
            'role_id' => '1'
        ]);
    }
}
