<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::redirect('/', '/login');
Route::redirect('/home', '/admin');
Auth::routes(['register' => false]);
Route::get('verify/resend', 'Auth\TwoFactorController@resend')->name('verify.resend');
Route::resource('verify', 'Auth\TwoFactorController')->only(['index', 'store']);

Route::group(['middleware' => ['auth', 'twofactor']], function () {
    Route::get('/', 'HomeController@index')->name('home')->middleware('verifyemail', 'lastsession');

    Route::get('/sessions', [
        'uses' => 'HomeController@sessions',
        'as' => 'sessions'
    ]);

    Route::get('home', [
        'uses' => 'HomeController@index',
        'as' => 'dashboard'
    ]);

    Route::get('/noVerified', [
        'uses' => 'HomeController@notVerified',
        'as' => 'notVerified'
    ]);
});